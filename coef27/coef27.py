# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 20:16:15 2020

@author: Ysaline REMAUD
"""
#importation des bibliothèques utiles pour la suite
import sys
import os
import easygui
import csv
from urllib.request import urlretrieve
import webbrowser


#Initialisation des répertoires locaux et distants où seront stockés des fichiers
#image et son
repertoire_pc = "C:/coef27/"
repertoire_git = "https://gitlab.com/Ysaline/ysalineproject/-/raw/master/coef27/"


#Création d'un répertoire coef 27 en local
os.makedirs(repertoire_pc, exist_ok=True)

#Chargement du fichier csv utilisé par la suite. Il est chargé dans un dictionnaire
fichier, entete = urlretrieve(repertoire_git+'persos.csv') 
persos = csv.DictReader(open(fichier, 'r'),delimiter=';')
persos_dict = [dict(ligne) for ligne in persos]


#Création de l'interface graphique
easygui.msgbox("bonjour :) ")
while 1:
    #Affichage d'une boîte interactive
    msg ="Qui est ton préféré ?" 
    title = "images" 
    choices = ["Bender le meilleur", "Kim la queen", "Bob le + fort"] 
    reply = easygui.buttonbox(msg, title, choices) #affichage de la boîte de dialogue et récupération du choix de l'utilisateur
    
    
    #Récupération de la réponse et association de celle-ci avec la ligne du fichier csv correspondante 
    if reply == "Bender le meilleur" : 
        nom = 'bender'
    elif reply == "Kim la queen" :
        nom = 'kim'
    elif reply == "Bob le + fort" :
        nom = 'bob'
    else :
        print('mauvais choix')
        
        
    #Recherche dans le dictionnaire le fichier son et image correspondant à la réponse
    for ligne in persos_dict:
            if ligne ['nom'] == nom : 
                image = ligne ['image']
                son = ligne ['son']


    #Indication du chemin pour trouver le fichier mp3 corresondant (utilisation de urlretrieve pour reccuperer le fichier de gitlab)
    url_son = repertoire_git + str(son)
    print(url_son)
    fichier_son = repertoire_pc + str(son)
    fichier, entete = urlretrieve(url_son,fichier_son)
    webbrowser.open(fichier_son) #lecture du fichier mp3
    
    #Indication du chemin pour trouver le fichier png correspondant (utilisation de urlretrieve pour reccuperer le fichier de gitlab) 
    url_image = repertoire_git + str(image)
    fichier_image = repertoire_pc + str(image)
    print(url_image)
    fichier, entete = urlretrieve(url_image,fichier_image)
    easygui.msgbox("Tu préfères " + str(reply), "Ce que tu as choisi",image=fichier_image) #Affichage d'un message avec le nom du personnage choisit et son image  
   
    
    #Le programme recommence ou s'arrête suivant le choix de l'utilisateur
    msg = "Tu veux changer ?"
    title = ""
    if easygui.ccbox(msg, title):     
        pass  
    else:
        sys.exit(0)           
