#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import random, threading
from tkinter import *

def simulation():
    def choc(balles):
        for i in range(0,len(balles)-1):
            for j in range(i+1, len(balles)):
                b1 = balles[i]
                b2 = balles[j]
                x1, x2 , y1, y2 = b1['x'], b2['x'], b1['y'], b2['y']
                vx1, vx2, vy1, vy2 = b1['vx'], b2['vx'], b1['vy'], b2['vy']
                dx = x2 - x1
                dy = y2 - y1
                dist = np.sqrt(dx**2 + dy**2)
                if dist <= 2.1*rayon:
                    nx, ny = dx/dist, dy/dist # vecteur normal unitaire
                    tx , ty = -ny, nx # vecteur tangente unitaire
                    vn1 = (vx1*nx + vy1*ny) # projection 1 normale
                    vt1 = (vx1*tx + vy1*ty) # projection 1 tangentielle
                    vn2 = (vx2*nx + vy2*ny) # projection 2 normale
                    vt2 = (vx2*tx + vy2*ty) # projection 2 tangentielle
                    vnP1, vnP2 = vn2, vn1 # les vecteurs normaux sont échangés comme un choc 1D
                    vtP1, vtP2 = vt1, vt2 # vecteurs tan inchangés
                    b1['vx'] = vnP1*nx + vt1*tx # v = vn + vt
                    b1['vy'] = vnP1*ny + vt1*ty
                    b2['vx'] = vnP2*nx + vt2*tx
                    b2['vy'] = vnP2*ny + vt2*ty
                    if b1['couleur'] == 'blue' and b2['couleur'] == 'green':
                            b1['couleur'] = 'green'
                            ls_malades.append('°')
                    if b2['couleur'] == 'blue' and b1['couleur'] == 'green':
                            b2['couleur'] = 'green'
                            ls_malades.append('°')

    d = 1 # figure entre -d et d en abscisses et ordonnées
    fig, ax = plt.subplots()
    plt.axis([-d, d, -d, d])

    # création de chaque balle
    ls_balles = []
    population = int(spin_population.get())
    malades_initial = int(spin_malades_initial.get())
    for i in range(0,population-malades_initial): # balles saines
        ls_balles.append({'x':float(f'{random.uniform(-d,d)}'),'y':float(f'{random.uniform(-d,d)}'), 'vx':0.039, 'vy':0.017, 'couleur': 'blue', 'maladie':0})
    for i in range(0, malades_initial): # balles malades
        ls_balles.append({'x':float(f'{random.uniform(-d,d)}'),'y':float(f'{random.uniform(-d,d)}'), 'vx':0.039, 'vy':0.017, 'couleur': 'green', 'maladie':0})

    # rayon de la balle : 
    rayon = d/25
    ls_malades = ['°'] * malades_initial

    # on dessine chaque première balle
    for b in ls_balles:
        b['cercle'] = plt.Circle((b['x'], b['y']), radius = rayon, color= b['couleur']) # dessine une série de ronds d'aire 1600 
        ax.add_artist(b['cercle'])

    while len(ls_malades) > 0:
        for b in ls_balles :
            # on efface la balle d'avant pour que une seule ne soit visible
            b['cercle'].set_visible(0)
            # la balle rebondit sur les bords
            if abs(b['x'] + b['vx']) > d - rayon: 
                b['vx'] = -b['vx']
            if abs(b['y'] + b['vy']) > d - rayon:
                b['vy'] = -b['vy']
            # ou contre une autre
            choc(ls_balles)
            # ou avance 
            b['x'] += b['vx']
            b['y'] += b['vy']
            # les balles malades s'immunisent au bout de cinq tours
            if b['couleur'] == 'green':
                b['maladie'] += 1
            if b['maladie'] == 5:
                b['couleur'] = 'purple'
                if len(ls_malades) != 0:
                    del ls_malades[0]
            # on dessine la balle 
            b['cercle'] = plt.Circle((b['x'], b['y']), radius = rayon, color= b['couleur']) # dessine une série de ronds d'aire 1600 
            ax.add_artist(b['cercle'])
            # on s'arrête entre chaque tracé de cercle
            plt.pause(0.01)
    # on montre le tout
    plt.show()

# fenêtre tkinter pour choisir les valeurs de base
fenetre = Tk()
fenetre.title("Valeurs de base")
fenetre.geometry("300x200")
fenetre.configure(background='white')

# nombre de personnes dans la population
frame_pop = Frame(fenetre, borderwidth=2, relief=GROOVE)
frame_pop.pack(side=TOP, padx=20, pady=20)
label_pop = Label(frame_pop, text='Population :')
label_pop.pack()
spin_population = Spinbox(frame_pop, from_=0, to=100)
spin_population.pack()

# nombre de personnes qui ont la maladie au départ
frame_mal = Frame(fenetre, borderwidth=2, relief=GROOVE)
frame_mal.pack(side=TOP, padx=20, pady=20)
label_mal = Label(frame_mal, text='Nombre de personnes malade :')
label_mal.pack()
spin_malades_initial = Spinbox(frame_mal, from_=0, to=100)
spin_malades_initial.pack()

# bouton valider qui ferme la fenêtre
but_valider=Button(fenetre, text="Valider", command=simulation)
but_valider.pack(side=BOTTOM)

fenetre.mainloop()